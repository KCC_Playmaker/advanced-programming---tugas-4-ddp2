/* Written by Kevin Christian Chandra - 1706039976
DDP 2 - C
GitLab Account: KCC_Playmaker
References:
Collaborator:
*/

import javari.animal.*;
import javari.reader.*;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class A3Festival {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
        System.out.print("... Opening default section database from data. ... ");
        String path = "../../../data";
        CsvReader[] readers = new CsvReader[4];
        while (true) {
            try {
                readers[0] = new RecordsReader(Paths.get(path, "animals_records.csv"));
                readers[1] = new AttractionsReader(Paths.get(path, "animals_attractions.csv"));
                readers[2] = new CategoriesReader(Paths.get(path, "animals_categories.csv"));
                readers[3] = new SectionsReader(Paths.get(path, "animals_categories.csv"));
                System.out.println("Success... System is populating data...\n");
                break;
            } catch (IOException e) {
                System.out.println("File not found or incorrect file!\n");
                System.out.print("Please provide the source data path: ");
                path = scan.nextLine();
            }
        }

        System.out.println("Found " + readers[3].countValidRecords()
                + " valid sections and " + readers[3].countInvalidRecords()
                + " invalid sections");
        System.out.println("Found " + readers[1].countValidRecords()
                + " valid attractions and " + readers[1].countInvalidRecords()
                + " invalid attractions");
        System.out.println("Found " + readers[2].countValidRecords()
                + " valid animal categories and " + readers[2].countInvalidRecords()
                + " invalid animal categories");
        System.out.println("Found " + readers[0].countValidRecords()
                + " valid animal records and " + readers[0].countInvalidRecords()
                + " invalid animal records");

        //Construct everything here
        //Animals
        String[][] animalsInputs = ((RecordsReader) readers[0]).getInputs();
        ArrayList<Animal> animals = new ArrayList<Animal>();
        for (String[] line : animalsInputs) {
            if (line[8].equals("valid")) {
                int id = Integer.parseInt(line[0]);
                Gender gender = Gender.parseGender(line[3]);
                double length = Double.parseDouble(line[4]);
                double weight = Double.parseDouble(line[5]);
                Condition condition = Condition.parseCondition(line[7]);
                if (line[1].equals("Hamster")
                        || line[1].equals("Lion")
                        || line[1].equals("Cat")
                        || line[1].equals("Whale")) {
                    boolean isPregnant = false;
                    if (line[6].equals("pregnant")) {
                        isPregnant = true;
                    }
                    animals.add(new Mammal(id, line[1], line[2], gender, length, weight, condition, isPregnant));
                } else if (line[1].equals("Eagle")
                        || line[1].equals("Parrot")) {

                    boolean isLayingEgg = false;
                    if (line[6].equals("laying egg")) {
                        isLayingEgg = true;
                    }
                    animals.add(new Aves(id, line[1], line[2], gender, length, weight, condition, isLayingEgg));
                } else if (line[1].equals("Snake")) {

                    boolean isTame;
                    if (line[6].equals("tame")) {
                        isTame = true;
                    } else if (line[6].equals("wild")) {
                        isTame = false;
                    } else {
                        isTame = true;
                    }
                    animals.add(new Reptile(id, line[1], line[2], gender, length, weight, condition, isTame));
                }
            }
        }

        //Attractions
        String[][] attractionsInputs = ((AttractionsReader) readers[1]).getInputs();

        System.out.println("\nWelcome to Javari Park Festival - Registration Service!\n");
    }
}
