/* Written by Kevin Christian Chandra - 1706039976
DDP 2 - C
GitLab Account: KCC_Playmaker
References: https://docs.oracle.com/javase/tutorial/java/javaOO/enum.html
Collaborator: Jonathan Christopher Jakub
*/

package javari.animal;

public class Mammal extends Animal {
	private boolean isPregnant;

	public Mammal (Integer id, String type, String name, Gender gender, double length,
                  double weight, Condition condition, boolean isPregnant) {
		super (id, type, name, gender, length, weight, condition);
		this.isPregnant = isPregnant;
	}

	public boolean isPregnant() {
		return this.isPregnant;
	}

	protected boolean specificCondition() {
		if (!this.isPregnant) {
			if (this.getType().equalsIgnoreCase("lion")) {
				if (this.getGender() == Gender.MALE) {
					return true;
				} else {
					return false;
				}
			} else {
				return true;
			}
		} else {
			return false;
		}
	}
}