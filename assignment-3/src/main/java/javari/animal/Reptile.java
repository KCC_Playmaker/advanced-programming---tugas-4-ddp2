/* Written by Kevin Christian Chandra - 1706039976
DDP 2 - C
GitLab Account: KCC_Playmaker
References: https://docs.oracle.com/javase/tutorial/java/javaOO/enum.html
Collaborator: Jonathan Christopher Jakub
*/

package javari.animal;

public class Reptile extends Animal {
	private boolean isTame;

	public Reptile (Integer id, String type, String name, Gender gender, double length,
                  double weight, Condition condition, boolean isTame) {
		super (id, type, name, gender, length, weight, condition);
		this.isTame = isTame;
	}

	public boolean isTame() {
		return this.isTame;
	}

	protected boolean specificCondition() {
		if (this.isTame) {
			return true;
		} else {
			return false;
		}
	}
}