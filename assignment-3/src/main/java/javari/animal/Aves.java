/* Written by Kevin Christian Chandra - 1706039976
DDP 2 - C
GitLab Account: KCC_Playmaker
References: https://docs.oracle.com/javase/tutorial/java/javaOO/enum.html
Collaborator: Jonathan Christopher Jakub
*/

package javari.animal;

public class Aves extends Animal {
	private boolean isLayingEgg;

	public Aves (Integer id, String type, String name, Gender gender, double length,
                  double weight, Condition condition, boolean isLayingEgg) {
		super (id, type, name, gender, length, weight, condition);
		this.isLayingEgg = isLayingEgg;
	}

	public boolean isLayingEgg() {
		return this.isLayingEgg;
	}

	protected boolean specificCondition() {
		if (!this.isLayingEgg) {
			return true;
		} else {
			return false;
		}
	}

}