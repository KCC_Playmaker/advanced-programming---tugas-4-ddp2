/* Written by Kevin Christian Chandra - 1706039976
DDP 2 - C
GitLab Account: KCC_Playmaker
References: https://docs.oracle.com/javase/tutorial/java/javaOO/enum.html
Collaborator: Jonathan Christopher Jakub
*/

package javari.park;

import javari.animal.*;
import java.util.List;

public class Attraction implements SelectedAttraction {
    private String name;
    private String type;
    private List<Animal> performers;

	public Attraction (String name, String type, List<Animal> performers) {
        this.name = name;
        this.type = type;
        this.performers = performers;
	}

	public String getName() {
		return this.name;
	}

	public String getType() {
		return this.type;
	}

	public List<Animal> getPerformers() {
		return this.performers;
	}

    public boolean addPerformer(Animal performer) {
    	if (this.name.equalsIgnoreCase("Circles of Fires")) {
    		if (!performer.getType().equalsIgnoreCase("Whale")
    			&& !performer.getType().equalsIgnoreCase("Lion")
    			&& !performer.getType().equalsIgnoreCase("Eagle")) {
    			return false;
    		} else {
    			this.performers.add(performer);
    			return true;
    		}
    	} else if (this.name.equalsIgnoreCase("Dancing Animals")) {
    		if (!performer.getType().equalsIgnoreCase("Cat")
    			&& !performer.getType().equalsIgnoreCase("Snake")
    			&& !performer.getType().equalsIgnoreCase("Hamster")
    			&& !performer.getType().equalsIgnoreCase("Parrot")) {
    			return false;
    		} else {
    			this.performers.add(performer);
    			return true;
    		}
    	} else if (this.name.equalsIgnoreCase("Counting Masters")) {
    		if (!performer.getType().equalsIgnoreCase("Whale")
    			&& !performer.getType().equalsIgnoreCase("Hamster")
    			&& !performer.getType().equalsIgnoreCase("Parrot")) {
    			return false;
    		} else {
    			this.performers.add(performer);
    			return true;
    		}
    	} else if (this.name.equalsIgnoreCase("Passionate Coders")) {
    		if (!performer.getType().equalsIgnoreCase("Cat")
    			&& !performer.getType().equalsIgnoreCase("Snake")
    			&& !performer.getType().equalsIgnoreCase("Hamster")) {
    			return false;
    		} else {
    			this.performers.add(performer);
    			return true;
    		}
    	} else {
    		return false;
    	}

    }
}