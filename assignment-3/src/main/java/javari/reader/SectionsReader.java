/* Written by Kevin Christian Chandra - 1706039976
DDP 2 - C
GitLab Account: KCC_Playmaker
References: https://docs.oracle.com/javase/tutorial/java/javaOO/enum.html
Collaborator: Jonathan Christopher Jakub
*/

package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.HashSet;

public class SectionsReader extends CsvReader {
    private final String[][] inputs;
    private HashSet<String> validSet = new HashSet<String>();
    private HashSet<String> invalidSet = new HashSet<String>();

    public SectionsReader(Path file) throws IOException {
        super(file);
        this.inputs = new String[this.lines.size()][4];
        for (int i = 0; i < this.lines.size(); i++) {
            String[] arr = this.lines.get(i).split(COMMA);
            for (int j = 0; j < arr.length; j++) {
                this.inputs[i][j] = arr[j];
            }
        }
    }

    public String[][] getInputs() {
        return this.inputs;
    }

    public long countValidRecords() {
        for (String[] input : this.inputs) {
            if (input[2].equals("Explore the Mammals")) {
                if (input[1].equals("mammals")) {
                    input[3] = "valid";
                }
            } else if (input[2].equals("World of Aves")) {
                if (input[1].equals("aves")) {
                    input[3] = "valid";
                }
            } else if (input[2].equals("Reptillian Kingdom")) {
                if (input[1].equals("reptiles")) {
                    input[3] = "valid";
                }
            }
        }

        for (String[] input : this.inputs) {
            if (input[3].equals("valid")) {
                this.validSet.add(input[2]);
            }
        }
        return this.validSet.size();
    }

    public long countInvalidRecords() {
        for (String[] input : this.inputs) {
            if (input[2].equals("Explore the Mammals")) {
                if (!input[1].equals("mammals")) {
                    input[3] = "invalid";
                }
            } else if (input[2].equals("World of Aves")) {
                if (!input[1].equals("aves")) {
                    input[3] = "invalid";
                }
            } else if (input[2].equals("Reptillian Kingdom")) {
                if (!input[1].equals("reptiles")) {
                    input[3] = "invalid";
                }
            } else {
                input[3] = "invalid";
            }
        }

        for (String[] input : this.inputs) {
            if (input[3].equals("invalid")) {
                this.invalidSet.add(input[2]);
            }
        }
        return this.invalidSet.size();
    }
}
