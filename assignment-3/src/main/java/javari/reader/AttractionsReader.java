/* Written by Kevin Christian Chandra - 1706039976
DDP 2 - C
GitLab Account: KCC_Playmaker
References: https://docs.oracle.com/javase/tutorial/java/javaOO/enum.html
Collaborator: Jonathan Christopher Jakub
*/

package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.HashSet;

public class AttractionsReader extends CsvReader {
    private final String[][] inputs;
    private HashSet<String> validSet = new HashSet<String>();
    private HashSet<String> invalidSet = new HashSet<String>();

    public AttractionsReader(Path file) throws IOException {
        super(file);
        this.inputs = new String[this.lines.size()][3];
        for (int i = 0; i < this.lines.size(); i++) {
            String[] arr = this.lines.get(i).split(COMMA);
            for (int j = 0; j < arr.length; j++) {
                this.inputs[i][j] = arr[j];
            }
        }
    }

    public String[][] getInputs() {
        return this.inputs;
    }

    public long countValidRecords() {
        for (String[] input : this.inputs) {
            if (input[1].equals("Circles of Fires")) {
                if (input[0].equals("Whale")
                        || input[0].equals("Lion")
                        || input[0].equals("Eagle")) {
                    input[2] = "valid";
                }
            } else if (input[1].equals("Dancing Animals")) {
                if (input[0].equals("Parrot")
                        || input[0].equals("Cat")
                        || input[0].equals("Hamster")
                        || input[0].equals("Snake")) {
                    input[2] = "valid";
                }
            } else if (input[1].equals("Counting Masters")) {
                if (input[0].equals("Parrot")
                        || input[0].equals("Hamster")
                        || input[0].equals("Whale")) {
                    input[2] = "valid";
                }
            } else if (input[1].equals("Passionate Coders")) {
                if (input[0].equals("Cat")
                        || input[0].equals("Snake")
                        || input[0].equals("Hamster")) {
                    input[2] = "valid";
                }
            }
        }

        for (String[] input : this.inputs) {
            if (input[2].equals("valid")) {
                this.validSet.add(input[1]);
            }
        }
        return this.validSet.size();
    }

    public long countInvalidRecords() {
        for (String[] input : this.inputs) {
            if ((input[0] == null)
                    || (input[1] == null)) {
                input[2] = "invalid";
                continue;
            }
            if (input[1].equals("Circles of Fires")) {
                if (!input[0].equals("Whale")
                        && !input[0].equals("Lion")
                        && !input[0].equals("Eagle")) {
                    input[2] = "invalid";
                }
            } else if (input[1].equals("Dancing Animals")) {
                if (!input[0].equals("Cat")
                        && !input[0].equals("Snake")
                        && !input[0].equals("Parrot")
                        && !input[0].equals("Hamster")) {
                    input[2] = "invalid";
                }
            } else if (input[1].equals("Counting Masters")) {
                if (!input[0].equals("Parrot")
                        && !input[0].equals("Hamster")
                        && !input[0].equals("Whale")) {
                    input[2] = "invalid";
                }
            } else if (input[1].equals("Passionate Coders")) {
                if (!input[0].equals("Cat")
                        && !input[0].equals("Hamster")
                        && !input[0].equals("Snake")) {
                    input[2] = "invalid";
                }
            }
        }

        for (String[] input : this.inputs) {
            if (input[2].equals("invalid")) {
                this.invalidSet.add(input[1]);
            }
        }
        return this.invalidSet.size();
    }
}
