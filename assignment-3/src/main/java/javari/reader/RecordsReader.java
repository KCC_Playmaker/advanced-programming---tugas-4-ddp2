/* Written by Kevin Christian Chandra - 1706039976
DDP 2 - C
GitLab Account: KCC_Playmaker
References: https://docs.oracle.com/javase/tutorial/java/javaOO/enum.html
Collaborator: Jonathan Christopher Jakub
*/

package javari.reader;

import java.nio.file.Path;
import java.io.IOException;

public class RecordsReader extends CsvReader {
    private final String[][] inputs;

    public RecordsReader(Path file) throws IOException {
        super(file);
        this.inputs = new String[this.lines.size()][9];
        for (int i = 0; i < this.lines.size(); i++) {
            String[] arr = this.lines.get(i).split(COMMA);
            for (int j = 0; j < arr.length; j++) {
                this.inputs[i][j] = arr[j];
            }
        }
    }

    public String[][] getInputs() {
        return this.inputs;
    }

    public long countValidRecords() {
        long counter = 0;
        for (String[] input : this.inputs) {
            if ((input[0] == null)
                    || (input[1] == null)
                    || (input[2] == null)
                    || (input[3] == null)
                    || (input[4] == null)
                    || (input[5] == null)
                    || (input[7] == null)) {
                continue;
            }
            if (input[3].equals("male")) {
                if (input[6].equals("pregnant")
                        || input[6].equals("laying egg")) {
                    continue;
                }

            }
            if (!(input[2].equals("Eagle") || input[2].equals("Snake"))) {
                if (input[6].equals("laying egg")) {
                    continue;
                }
            }
            if (input[2].equals("Eagle") || input[2].equals("Snake")) {
                if (input[6].equals("pregnant")) {
                    continue;
                }
            }
            input[8] = "valid";
            counter += 1;
        }
        return counter;
    }

    public long countInvalidRecords() {
        long counter = 0;
        for (String[] input : this.inputs) {
            if ((input[0] == null)
                    || (input[1] == null)
                    || (input[2] == null)
                    || (input[3] == null)
                    || (input[4] == null)
                    || (input[5] == null)
                    || (input[7] == null)) {
                input[8] = "invalid";
                counter += 1;
                continue;
            }
            if (input[3].equals("male")) {
                if (input[6].equals("pregnant")
                        || input[6].equals("laying egg")) {
                    input[8] = "invalid";
                    counter += 1;
                    continue;
                }
            }
            if (!input[2].equals("Eagle") && !input[2].equals("Snake")) {
                if (input[6].equals("laying egg")) {
                    input[8] = "invalid";
                    counter += 1;
                    continue;
                }
            }
            if (input[2].equals("Eagle") || input[2].equals("Snake")) {
                if (input[6].equals("pregnant")) {
                    input[8] = "invalid";
                    counter += 1;
                }
            }
        }
        return counter;
    }
}