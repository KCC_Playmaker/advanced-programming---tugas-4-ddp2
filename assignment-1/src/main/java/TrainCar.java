/* Written by Kevin Christian Chandra - 1706039976
DDP 2 - C
GitLab Account: KCC_Playmaker
References: -
*/

public class TrainCar {

    //weight of empty traincar in kilograms
    public static final double EMPTY_WEIGHT = 20;

    //instance variables or attributes
    WildCat cat;
    TrainCar nextCar;

    
    //constructor with only WildCat parameter
    public TrainCar(WildCat cat) { 
        this(cat, null);
    }


    //constructor with WildCat and next TrainCar parameters
    public TrainCar(WildCat cat, TrainCar next) {
        this.cat = cat;
        this.nextCar = next;
    }


    //method to count the total weight of all cars
    public double computeTotalWeight() {
        double result;
        if (nextCar != null) {
            result = TrainCar.EMPTY_WEIGHT + this.cat.weight + this.nextCar.computeTotalWeight();
        } else {
            result = TrainCar.EMPTY_WEIGHT + this.cat.weight;
        }
        return result;
    }


    //method to count the total mass index of all cats on the train
    public double computeTotalMassIndex() {
        double result;
        if (nextCar != null) {
            result = this.cat.computeMassIndex() + this.nextCar.computeTotalMassIndex();
        } else {
            result = this.cat.computeMassIndex();
        }
        return result;
    }


    //method to print info about the traincars and the cats inside
    public void printCar() {
        if (nextCar == null) {
            System.out.println("(" + this.cat.name + ")");
        } else {
            System.out.print("(" + this.cat.name + ")--");
            this.nextCar.printCar();
        }
    }
}
