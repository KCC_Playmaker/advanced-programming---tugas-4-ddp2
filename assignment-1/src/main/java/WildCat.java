/* Written by Kevin Christian Chandra - 1706039976
DDP 2 - C
GitLab Account: KCC_Playmaker
References: -
*/

public class WildCat {

    //instance variables or attributes
    String name; //cat's name
    double weight; //cat's weight in kilograms
    double length; //cat's length in centimeters


    public WildCat(String name, double weight, double length) { //constructor
        this.name = name;
        this.weight = weight;
        this.length = length;
    }


    //method to count the cat's mass index
    public double computeMassIndex() {
        double lengthInMetre = this.length / 100.0;
        double massIndex = this.weight / (lengthInMetre * lengthInMetre);
        return massIndex;
    }
}
