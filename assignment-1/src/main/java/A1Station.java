/* Written by Kevin Christian Chandra - 1706039976
DDP 2 - C
GitLab Account: KCC_Playmaker
References: -
*/

import java.util.Scanner; //imports the scanner to read inputs

public class A1Station {

    //limit of total weight of the train in kilograms
    private static final double THRESHOLD = 250;

    private static TrainCar recentCar; //stores the most recent car created
    private static int catCounter = 0; //stores the numbers of cats already on train
    

    //method to depart the train when the total weight exceeds the THRESHOLD
    public static void depart() {
        double averageMassIndex = (double) recentCar.computeTotalMassIndex() / catCounter;
        
        String catCategory;

        //sets the category of the cats based on average mass index
        if (averageMassIndex < 18.5) {
            catCategory = "*underweight*";
        } else if (averageMassIndex >= 18.5 && averageMassIndex < 25.0) {
            catCategory = "*normal*";
        } else if (averageMassIndex >= 25.0 && averageMassIndex < 30.0) {
            catCategory = "*overweight*";
        } else {
            catCategory = "*obese*";
        }

        System.out.println("The train departs to Javari Park");
        System.out.print("[LOCO]<--");
        recentCar.printCar();
        System.out.format("Average mass index of all cats: %.2f\n", averageMassIndex);
        System.out.println("In average, the cats in the train are " + catCategory);

        recentCar = null; //empties the station
        catCounter = 0; //no cats left after departure
    }


    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        //reads user's input of numbers of cats
        int input1 = Integer.parseInt(reader.nextLine());

        
        //asks inputs as many as input1 about the cats
        for (int i = 0; i < input1; i++) {
            String input2 = reader.nextLine();
            String[] input2Arr = input2.split(",");
            double catWeight = Double.parseDouble(input2Arr[1]);
            double catLength = Double.parseDouble(input2Arr[2]);

            WildCat newCat = new WildCat(input2Arr[0], catWeight, catLength);
            catCounter += 1;

            //creates a new traincar for a new cat
            if (recentCar == null) {
                recentCar = new TrainCar(newCat);
            } else {
                recentCar = new TrainCar(newCat, recentCar);
                if (recentCar.computeTotalWeight() > 250) {
                    depart();
                }
            }
        }

        //if there is no more inputs and there is still traincars left, depart the train
        if (recentCar != null) {
            depart();
        }

        reader.close(); //closes the reader
    }
}
