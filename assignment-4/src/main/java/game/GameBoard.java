package game;

import game.listener.CardListener;
import game.listener.TimerListener;

import java.awt.*;
import javax.swing.*;
import javax.imageio.ImageIO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.io.File;
import java.io.IOException;

/**
 * Class representing the game board extending JPanel
 * The panel on which the cards are placed
 * 
 * @author Kevin Christian Chandra - 1706039976
 * Collaborator: Jonathan Christopher Jakub and Mikhael
 */
public class GameBoard extends JPanel {

    /**
     * The hashmap to store the true image of the cards as value and the image's name as key
     */
    private HashMap<String, Image> trueImages = new HashMap<String, Image>();

    /**
     * The hashmap to store the back image of the cards as value and the image's name as key
     */
    private HashMap<String, Image> backImages = new HashMap<String, Image>();

    /**
     * The arraylist to store the keys of the true images for iteration purpose
     */
    private ArrayList<String> trueImgKeys;

    /**
     * The arraylist to store the cards of this board
     */
    private ArrayList<GameCard> cards;

    /**
     * The arraylist to store currently opened cards
     */
    private ArrayList<GameCard> openedCards;

    /**
     * The game frame on which this board is placed on
     */
    private MemoryGame game;

    /**
     * The number of  player's tries
     */
    private int numOfTries;

    /**
     * The number of pair of cards that the player successfully paired
     */
    private int numOfPaired;

    /**
     * The action listener for the cards
     */
    private CardListener cardListener;

    /**
     * The timer to for setting delay of 500ms
     * Its timer listener listens to actions on this board
     */
    final Timer timer = new Timer(500, new TimerListener(this));
    
    /**
     * The constructor of the game board
     * Generate the cards to be placed on it
     * @param game The game frame on which this board is placed on
     */
    public GameBoard(MemoryGame game) {
        this.game = game;
        this.numOfTries = 0;
        this.trueImgKeys = new ArrayList<String>();
        this.openedCards = new ArrayList<GameCard>();
        this.cards = new ArrayList<GameCard>();
        this.cardListener = new CardListener(this);
        this.setSize(900, 900);
        this.setLayout(new GridLayout(6, 6));
        this.setVisible(true);

        this.setBackAndTrueImages(); //Calls the method to set all images needed for the cards

        //using the trueImgKeys arraylist to iterate through the HashMaps
        for (int i = 0; i < 2; i++) {
            for (String key : this.trueImgKeys) {
                GameCard card = new GameCard(this.backImages.get("Layton.png"), this.trueImages.get(key), key);//new GameCard
                card.setActionCommand(key); //debugging
                this.cards.add(card);
            }
        }
        Collections.shuffle(this.cards); //shuffle the cards before putting them on the board

        //puts the cards on board
        for (int i = 0; i < 36; i++) {
            GameCard card = this.cards.get(i);
            card.addActionListener(this.cardListener);
            this.add(card);
        }
    }

    /**
     * Method to get this board's card listener
     * @return this board's card listener
     */
    public CardListener getCardListener() {
        return this.cardListener;
    }

    /**
     * Method to get this board's game frame
     * @return this board's game frame
     */
    public MemoryGame getGameFrame() {
        return this.game;
    }

    /**
     * Method to get the arraylist of this board's cards
     * @return the arraylist of this board's cards
     */
    public ArrayList<GameCard> getCards() {
        return this.cards;
    }

    /**
     * Method to get the arraylist of this board's currently opened cards
     * @return the arraylist of this board's currently opened cards
     */
    public ArrayList<GameCard> getOpenedCards() {
        return this.openedCards;
    }

    /**
     * Method to get this board's timer
     * @return this board's timer
     */
    public Timer getTimer() {
        return this.timer;
    }

    /**
     * Method to get this board's number of paired cards
     * @return this board's number of paired cards
     */
    public int getPairedNum() {
        return this.numOfPaired;
    }

    /**
     * Method to increase the number of player's tries
     * Updates the frame's labelTries
     */
    public void increaseTry() {
        this.numOfTries += 1;
        this.game.setLabelTries(this.numOfTries);
    }

    /**
     * Method to increase the number of paired cards
     */
    public void increasePaired() {
        this.numOfPaired += 1;
    }

    /**
     * Method to reset the cards on board
     * Called by the frame's reset method
     */
    public void resetCardsOnBoard() {
        this.numOfTries = 0;
        this.numOfPaired = 0;
        this.removeAll();
        this.openedCards.clear();
        Collections.shuffle(this.cards);
        for (int i = 0; i < 36; i++) {
            GameCard card = this.cards.get(i);
            card.setIcon(card.getBackIcon());
            card.setBackground(new Color(255, 174, 44));
            card.setEnabled(true);
            card.setVisible(true);
        }
        for (int i = 0; i < 36; i++) {
            GameCard card = this.cards.get(i);
            this.add(card);
        }
    }

    /**
     * Method to set the background image of the board
     * @param g some graphics object
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Dimension panelSize = this.getSize();
        Image scaledImage = this.readImage("../../../img/LaytonCharacters.jpg").getScaledInstance(panelSize.width, panelSize.height, java.awt.Image.SCALE_SMOOTH);
        g.drawImage(scaledImage, 0, 0, null);
    }

    /**
     * method to read an image from image path string
     * uses the ImageIO.read(File file) method which throws IOException and IllegalArgumentException
     * @param imageName the name or path string of the image to be read
     * @return an image object of the read name, null if IOException or IllegalArgumentException caught
     */
    public Image readImage (String imageName) {
        Image img = null;
        try {
            img = ImageIO.read(new File(imageName));
        } catch (IOException e) {
            System.out.println("Error occur during reading image with the name of " + imageName);
        } catch (IllegalArgumentException a) {
            System.out.println("No image with the name of " + imageName);
        }
        return img;
    }

    /**
     * Method to set all the back and true images to their hashmaps
     * Adds the keys of the trueImages hashmap into trueImgKeys arraylist
     * Calls the readImage(String imageName) method
     */
    private void setBackAndTrueImages() {
        this.backImages.put("Layton.png", this.readImage("../../../img/Layton.png"));

        this.trueImages.put("Barton.png", this.readImage("../../../img/Barton.png"));
        this.trueImgKeys.add("Barton.png");
        this.trueImages.put("Chelmey.png", this.readImage("../../../img/Chelmey.png"));
        this.trueImgKeys.add("Chelmey.png");
        this.trueImages.put("Dahlia.png", this.readImage("../../../img/Dahlia.png"));
        this.trueImgKeys.add("Dahlia.png");
        this.trueImages.put("DeanDelmona.png", this.readImage("../../../img/DeanDelmona.png"));
        this.trueImgKeys.add("DeanDelmona.png");
        this.trueImages.put("Descole.png", this.readImage("../../../img/Descole.png"));
        this.trueImgKeys.add("Descole.png");
        this.trueImages.put("Dimitri.png", this.readImage("../../../img/Dimitri.png"));
        this.trueImgKeys.add("Dimitri.png");
        this.trueImages.put("DonPaolo.png", this.readImage("../../../img/DonPaolo.png"));
        this.trueImgKeys.add("DonPaolo.png");
        this.trueImages.put("Emmy.png", this.readImage("../../../img/Emmy.png"));
        this.trueImgKeys.add("Emmy.png");
        this.trueImages.put("Flora.png", this.readImage("../../../img/Flora.png"));
        this.trueImgKeys.add("Flora.png");
        this.trueImages.put("Grosky.png", this.readImage("../../../img/Grosky.png"));
        this.trueImgKeys.add("Grosky.png");
        this.trueImages.put("Hershel.png", this.readImage("../../../img/Hershel.png"));
        this.trueImgKeys.add("Hershel.png");
        this.trueImages.put("Herzen.png", this.readImage("../../../img/Herzen.png"));
        this.trueImgKeys.add("Herzen.png");
        this.trueImages.put("Katia.png", this.readImage("../../../img/Katia.png"));
        this.trueImgKeys.add("Katia.png");
        this.trueImages.put("Katrielle.png", this.readImage("../../../img/Katrielle.png"));
        this.trueImgKeys.add("Katrielle.png");
        this.trueImages.put("Keats.png", this.readImage("../../../img/Keats.png"));
        this.trueImgKeys.add("Keats.png");
        this.trueImages.put("Luke.png", this.readImage("../../../img/Luke.png"));
        this.trueImgKeys.add("Luke.png");
        this.trueImages.put("Riddleton.png", this.readImage("../../../img/Riddleton.png"));
        this.trueImgKeys.add("Riddleton.png");
        this.trueImages.put("Schrader.png", this.readImage("../../../img/Schrader.png"));
        this.trueImgKeys.add("Schrader.png");
    }
}