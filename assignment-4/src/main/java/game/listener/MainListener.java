package game.listener;

import game.MemoryGame;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JButton;

/**
 * Class representing the main listener for "Play again?" and "Exit" buttons
 * Implements the ActionListener interface
 * Used on MemoryGame's button panel
 *
 * @author Kevin Christian Chandra - 1706039976
 * Collaborator: Jonathan Christopher Jakub and Mikhael
 */
public class MainListener implements ActionListener {

    /**
     * The game frame on which this listener is listening for actions
     */
    private MemoryGame game;

    /**
     * The constructor of the main listener
     * @param game The game frame on which this listener is listening for actions
     */
    public MainListener (MemoryGame game) {
        this.game = game;
    }

    /**
     * Method to respond to action listened by this listener
     * Override from the interface ActionListener
     * This method will respond based on the clicked button
     * "Play again?" will reset the game
     * "Exit" will close the game
     * @param e The Action event listened
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String actionCommand = e.getActionCommand();

        if (actionCommand.equals("Play again?")) {
            this.game.reset();
        } else if (actionCommand.equals("Exit")) {
            System.exit(0);
        }
    }
}