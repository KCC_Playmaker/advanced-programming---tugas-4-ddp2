package game.listener;

import game.GameBoard;
import game.GameCard;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.util.ArrayList;
import javax.swing.Timer;

/**
 * Class representing the card listener on the game board
 * Implements the ActionListener interface
 *
 * @author Kevin Christian Chandra - 1706039976
 * Collaborator: Jonathan Christopher Jakub and Mikhael
 */
public class CardListener implements ActionListener {

    /**
     * The game board on which this listener is listening for actions
     */
    private GameBoard gameBoard;

    /**
     * Constructor of the card listener
     * @param board the gameBoard on which this listener is listening for actions
     */
    public CardListener (GameBoard board) {
        this.gameBoard = board;
    }

    /**
     * Method to respond to action listened by this listener
     * Override from the interface ActionListener
     * This method will flip the card if currently opened card(s) are less than 2
     * This method only flip cards that is closed before
     * Already opened card won't react to any clicks
     *
     * Then the timer will activate so that the second cards won't be closed for a moment delay
     * @param e The Action event listened
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        GameCard clickedCard = (GameCard) e.getSource();
        ArrayList<GameCard> openedCards = this.gameBoard.getOpenedCards();

        if (openedCards.size() < 2) {
            //If currently opened cards is less than 2, if it is still closed, open it
            if (clickedCard.getIcon() == clickedCard.getBackIcon()) {
                openedCards.add(clickedCard);
                clickedCard.swapIcon();
            }
        } else { //There are already two opened cards, don't do anything yet
            return;
        }

        Timer timer = this.gameBoard.getTimer();
        //Starts the timer
        if (openedCards.size() == 1) {
            timer.start();
        } else if (openedCards.size() == 2) {
            timer.restart();
        }
        timer.setRepeats(false); //So that no repeated respond happened
    }
}
