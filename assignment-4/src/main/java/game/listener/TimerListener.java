package game.listener;

import game.GameBoard;
import game.GameCard;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.awt.Image;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.Timer;
import javax.swing.JOptionPane;


/**
 * Class representing the timer listener for timer
 * Implements the ActionListener interface
 * Used on GameBoard as timer for the board's cards
 *
 * @author Kevin Christian Chandra - 1706039976
 * Collaborator: Jonathan Christopher Jakub and Mikhael
 */
public class TimerListener implements ActionListener {

    /**
     * The game board on which this listener is waiting for the timer
     */
    private GameBoard gameBoard;

    /**
     * The constructor for the TimerListener
     * @param gameBoard The game board on which this listener is waiting for the timer
     */
    public TimerListener(GameBoard gameBoard) {
        this.gameBoard = gameBoard;
    }

    /**
     * Method to respond to action listened by this listener after the timer's done running
     * Override from the interface ActionListener
     * This method will show respond after two cards are opened
     * If those cards are the same, take them away
     * If those cards are not the same, flip them back
     *
     * If all cards has been paired, show a message-question box
     * to ask whether the player wants to play again
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (this.gameBoard.getOpenedCards().size() == 2) {
            GameCard card1 = this.gameBoard.getOpenedCards().get(0);
            GameCard card2 = this.gameBoard.getOpenedCards().get(1);
            if (card1.getImageName().equals(card2.getImageName())) {
                //The opened cards are the same, then take them away
                card1.setEnabled(false);
                card1.setVisible(false);
                card2.setEnabled(false);
                card2.setVisible(false);
                this.gameBoard.increasePaired(); //Increase number of paired cards!
            } else {
                //Not the same cards, flip them back
                card1.swapIcon();
                card2.swapIcon();
            }
            this.gameBoard.getOpenedCards().clear(); //Clears the opened cards arraylist --> No cards are opened
            this.gameBoard.increaseTry(); //Increases the number of player's tries
        }

        if (this.gameBoard.getPairedNum() == 18) {
            //Puzzle solved! Generate message box
            Image img = this.gameBoard.readImage("../../../img/MiniLayton.png");
            Image scaledImg = img.getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH);
            ImageIcon icon = new ImageIcon(scaledImg);
            int wannaReplay = JOptionPane.showOptionDialog(this.gameBoard, "\"Creativity and ingenuity are the key to success.\" - Professor Hershel Layton\nDo you want to play again?", "Puzzle Solved!", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, icon, new String[]{"Yes", "No"}, null);

            if (wannaReplay == JOptionPane.YES_OPTION) {
                //If the player chooses "Yes" then reset the game
                this.gameBoard.getGameFrame().reset();
            }
        }
    }
}