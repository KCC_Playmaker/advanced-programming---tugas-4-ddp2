package game;

import java.awt.*;
import javax.swing.*;

/**
 * Class representing the game cards extending JButton
 * 
 * @author Kevin Christian Chandra - 1706039976
 * Collaborator: Jonathan Christopher Jakub and Mikhael
 */
public class GameCard extends JButton {

    /**
     * The image icon of the card's back side
     */
    private ImageIcon backIcon;

    /**
     * The image icon of the card's true side
     */
    private ImageIcon trueIcon;

    /**
     * The name of the true image representing the card
     */
    private String imageName;

    /**
     * The constructor of the card
     * @param backImage the image object to be the back image icon of the card
     * @param trueImage the image object to be the true image icon of the card
     * @param imageName the name of the true image to represent the card's identity
     */
    public GameCard(Image backImage, Image trueImage, String imageName) {
        this.imageName = imageName;
        this.setSize(150, 150);
        Dimension buttonSize = this.getSize(); //for scaling the image
        Image scaledBack = backImage.getScaledInstance(buttonSize.width, buttonSize.height, java.awt.Image.SCALE_SMOOTH);
        Image scaledTrue = trueImage.getScaledInstance(buttonSize.width, buttonSize.height, java.awt.Image.SCALE_SMOOTH);

        this.backIcon = new ImageIcon(scaledBack);
        this.trueIcon = new ImageIcon(scaledTrue);

        this.setIcon(this.backIcon);
        this.setBackground(new Color(255, 174, 44));
        this.setVisible(true);
    }

    /**
     * Method to get the card's back icon
     * @return the card's back icon
     */
    public ImageIcon getBackIcon() {
        return this.backIcon;
    }

    /**
     * Method to get the card's true icon
     * @return the card's true icon
     */
    public ImageIcon getTrueIcon() {
        return this.trueIcon;
    }

    /**
     * Method to get the card's true image name
     * @return the card's true image name
     */
    public String getImageName() {
        return this.imageName;
    }

    /**
     * Method to swap the card's current player-facing icon from backIcon to trueIcon
     * or from trueIcon to backIcon
     * Simply "flipping" the card
     */
    public void swapIcon() {
        if (this.getIcon() == this.backIcon) {
            this.setIcon(this.trueIcon);
        } else {
            this.setIcon(this.backIcon);
        }
    }
}