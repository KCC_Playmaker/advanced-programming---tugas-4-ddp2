package game;

import game.listener.MainListener;

import java.awt.*;
import javax.swing.*;
import javax.imageio.ImageIO;

import java.util.Collections;
import java.io.File;
import java.io.IOException;

/**
 * Class representing the main frame of the game
 *
 * @author Kevin Christian Chandra - 1706039976
 * Collaborator: Jonathan Christopher Jakub and Mikhael
 */
public class MemoryGame extends JFrame {

    /**
     * The game board on this frame
     */
    private GameBoard gameBoard;

    /**
     * The panel for "Play again?" and "Exit" buttons
     */
    // private JPanel buttonPanel;

    /**
     * The label in the downmost part of the frame representing the number of the player's tries
     */
    private JLabel labelTries;

    /**
     * The listener of the "Play again?" and "Exit" buttons
     */
    private MainListener mainListener;

    /**
     * Costructor
     * Uses the GridBagLayout for components
     * Constructs the main frame, game board with cards, the buttons in panel, and the labelTries
     */
    public MemoryGame() {
        this.setTitle("PUZZLE!! NO.TP4 - Flip and Match");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(new Dimension(900, 1000));
        this.setLayout(new GridBagLayout());
        this.setVisible(true);

        Image img = this.readImage("../../../img/MiniLayton.png");
        this.setIconImage(img);

        //First row for game board
        GridBagConstraints con1 = new GridBagConstraints();
        con1.insets.top = 0;
        con1.insets.bottom = 0;
        con1.insets.left = 0;
        con1.insets.right = 0;
        con1.fill = GridBagConstraints.BOTH;
        con1.weightx = 1;
        con1.weighty = 1;
        con1.gridy = 0;
        con1.anchor = GridBagConstraints.CENTER;
        con1.gridx = 0;
        this.gameBoard = new GameBoard(this); //the game board for the cards
        this.add(this.gameBoard, con1);


        //Second row for buttons
        JButton buttonPlayAgain = new JButton("Play again?"); //button to reset the game and play again
        buttonPlayAgain.setSize(250, 150);
        JButton buttonExit = new JButton("Exit"); //button to exit the game
        buttonExit.setSize(100, 50);

        //creates panel for buttons
        // this.buttonPanel = new JPanel();
        // buttonPanel.setLayout(new GridBagLayout());
        // buttonPanel.setSize(600, 50);
        // GridBagConstraints constraints = new GridBagConstraints();
        // constraints.insets.top = 0;
        // constraints.insets.bottom = 0;
        // constraints.insets.left = 0;
        // constraints.insets.right = 0;
        // constraints.fill = GridBagConstraints.NONE;
        // constraints.weightx= 1;
        // constraints.gridy = 0;

        // constraints.anchor = GridBagConstraints.EAST;
        // constraints.gridx = 0;
        // buttonPanel.add(buttonPlayAgain, constraints);

        // constraints.anchor = GridBagConstraints.WEST;
        // constraints.gridx = 1;
        // buttonPanel.add(buttonExit, constraints);

        //adds listener for buttons
        this.mainListener = new MainListener(this);
        buttonPlayAgain.addActionListener(this.mainListener);
        buttonExit.addActionListener(this.mainListener);

        //adds panel of button to frame
        con1.insets.top = 10;
        con1.weighty = 0;
        con1.gridy = 1;
        this.add(this.buttonPanel, con1);

        //Third row for label
        this.labelTries = new JLabel("Number of tries: 0", JLabel.CENTER);
        con1.insets.top = 0;
        con1.gridy = 2;
        this.add(this.labelTries, con1);
    }

    /**
     * method to set the labelTries
     * @param numOfTries the current number of player's tries
     */
    public void setLabelTries(int numOfTries) {
        this.labelTries.setText("Number of tries: " + numOfTries);
    }

    /**
     * method to reset the game
     */
    public void reset() {
        this.gameBoard.setVisible(false);
        this.gameBoard.resetCardsOnBoard(); //Calls the reset method on the game board
        this.gameBoard.setVisible(true);
        this.setLabelTries(0);
    }

    /**
     * method to read an image from image path string
     * uses the ImageIO.read(File file) method which throws IOException and IllegalArgumentException
     * @param imageName the name or path string of the image to be read
     * @return an image object of the read name, null if IOException or IllegalArgumentException caught
     */
    public Image readImage (String imageName) {
        Image img = null;
        try {
            img = ImageIO.read(new File(imageName));
        } catch (IOException e) {
            System.out.println("Error occur during reading image with the name of " + imageName);
        } catch (IllegalArgumentException a) {
            System.out.println("No image with the name of " + imageName);
        }
        return img;
    }
}