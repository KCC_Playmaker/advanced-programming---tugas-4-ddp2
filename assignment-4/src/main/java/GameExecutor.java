import game.MemoryGame;

/**
 * Class representing the executor of the game
 */
public class GameExecutor {

    /**
     * Main method to start the game
     */
    public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new MemoryGame();
			}
		});
    }
}