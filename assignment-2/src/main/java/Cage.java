public class Cage {
    private Animal insider;
    private String size;
    private int length;
    private int width;
    private boolean isOutdoor;

    public Cage(Animal animal, int length) {
        this.insider = animal;
        if (animal.isWild() == true) {
            this.isOutdoor = true;
            this.length = 120;
            if (animal.getLength() < 75) {
                this.size = "A";
                this.width = 120;
            } else if (animal.getLength() >= 75 && animal.getLength() <= 90) {
                this.size = "B";
                this.width = 150;
            } else {
                this.size = "C";
                this.width = 180;
            }
        } else {
            this.isOutdoor = false;
            this.length = 60;
            if (animal.getLength() < 45) {
                this.size = "A";
                this.width = 60;
            } else if (animal.getLength() >= 45 && animal.getLength() <= 60) {
                this.size = "B";
                this.width = 90;
            } else {
                this.size = "C";
                this.width = 120;
            }
        }

    }

    public Animal getAnimal() {
        return this.insider;
    }

    public String getSize() {
        return this.size;
    }

    public int getLength() {
        return this.length;
    }

    public int getWidth() {
        return this.width;
    }

    public void printDimension() {
        System.out.println(this.length + " cm x " + this.width + " cm");
    }

    public boolean isOutdoor() {
        return this.isOutdoor;
    }
    
}