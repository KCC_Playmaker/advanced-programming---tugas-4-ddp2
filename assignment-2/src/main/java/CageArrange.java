import java.util.ArrayList;

public class CageArrange {

    public static ArrayList<ArrayList<Cage>> arrange(Cage[] listOfCages) {
        int cagesPerLv = listOfCages.length / 3;
        if (cagesPerLv == 0) {
            cagesPerLv = 1;
        }

        ArrayList<ArrayList<Cage>> cages = new ArrayList<ArrayList<Cage>>();
        for (int lv = 0; lv < 3; lv++) {
            cages.add(new ArrayList<Cage>());
        }

        for (int i = 0, lv = 0; i < listOfCages.length; i++) {
            if ((i % cagesPerLv == 0) && (i > 0) && (lv < 2)) {
                lv++;
            }
            cages.get(lv).add(listOfCages[i]);
        }

        System.out.print("location: ");
        if (listOfCages[0].isOutdoor()) {
            System.out.println("outdoor");
        } else {
            System.out.println("indoor");
        }
        for (int i = 0; i < cages.size(); i++) {
            System.out.print("level " + (3-i) + ": ");
            int levelNow = 3 - i - 1;
            if (cages.get(levelNow) != null) {
                for (Cage var : cages.get(levelNow)) {
                    System.out.print(var.getAnimal().getName() + " ("
                        + var.getAnimal().getLength() + " - "
                        + var.getSize() + "), ");
                }
            }
            System.out.println("");
        }
        System.out.println("");
        
        //rearrangement
        cages.add(0, cages.get(2));
        cages.remove(3);
        for (int lev = 0; lev < cages.size(); lev++) {
            int levSize = cages.get(lev).size();
            for (int i = 0; i < levSize; i++) {
                Cage lastCage = cages.get(lev).get(levSize-1);
                cages.get(lev).add(i, lastCage);
                cages.get(lev).remove(levSize);
            }
        }

        System.out.println("After rearrangement...");
        for (int i = 0; i < cages.size(); i++) {
            System.out.print("level " + (3-i) + ": ");
            int levelNow = 3 - i - 1;
            if (cages.get(levelNow) != null) {
                for (Cage var : cages.get(levelNow)) {
                    System.out.print(var.getAnimal().getName() + " ("
                        + var.getAnimal().getLength() + " - "
                        + var.getSize() + "), ");
                }
            }
            System.out.println("");
        }
        System.out.println("");

        return cages;
    }
}