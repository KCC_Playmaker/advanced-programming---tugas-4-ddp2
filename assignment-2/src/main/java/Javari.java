import java.util.Scanner;
import java.util.ArrayList;

public class Javari {

    public static void main (String[] args) {
        Scanner reader = new Scanner(System.in);
        
        System.out.print("Welcome to Javari Park!\n"
            + "Input the number of animals\n"
            + "cat: ");
        
        int catNum = reader.nextInt();
        reader.nextLine();
        Cage[] catCages = new Cage[catNum];
        if (catNum != 0) {
            System.out.println("Provide the information of cat(s):");
            String catInput = reader.nextLine();
            String[] catInputList1 = catInput.split(",");
            Cat[] cats = new Cat[catNum];
            for (int i = 0; i < catInputList1.length; i++) {
                String[] arr = catInputList1[i].split("\\|");
                String name = arr[0];
                int length = Integer.parseInt(arr[1]);
                cats[i] = new Cat(name, length);
            }
            
            for (int i = 0; i < cats.length; i++) {
                catCages[i] = new Cage(cats[i], cats[i].getLength());
            }
        }
        
        System.out.print("lion: ");
        int lionNum = reader.nextInt();
        reader.nextLine();
        Cage[] lionCages = new Cage[lionNum];
        if (lionNum != 0) {
            System.out.println("Provide the information of lion(s):");
            String lionInput = reader.nextLine();
            String[] lionInputList1 = lionInput.split(",");
            Lion[] lions = new Lion[lionNum];
            for (int i = 0; i < lionInputList1.length; i++) {
                String[] arr = lionInputList1[i].split("\\|");
                String name = arr[0];
                int length = Integer.parseInt(arr[1]);
                lions[i] = new Lion(name, length);
            }
            
            for (int i = 0; i < lions.length; i++) {
                lionCages[i] = new Cage(lions[i], lions[i].getLength());
            }
        }
        
        System.out.print("eagle: ");
        int eagleNum = reader.nextInt();
        reader.nextLine();
        Cage[] eagleCages = new Cage[eagleNum];
        if (eagleNum != 0) {
            System.out.println("Provide the information of eagle(s):");
            String eagleInput = reader.nextLine();
            String[] eagleInputList1 = eagleInput.split(",");
            Eagle[] eagles = new Eagle[eagleNum];
            for (int i = 0; i < eagleInputList1.length; i++) {
                String[] arr = eagleInputList1[i].split("\\|");
                String name = arr[0];
                int length = Integer.parseInt(arr[1]);
                eagles[i] = new Eagle(name, length);
            }
            
            for (int i = 0; i < eagles.length; i++) {
                eagleCages[i] = new Cage(eagles[i], eagles[i].getLength());
            }
        }

        System.out.print("parrot: ");
        int parrotNum = reader.nextInt();
        reader.nextLine();
        Cage[] parrotCages = new Cage[parrotNum];
        if (parrotNum != 0) {
            System.out.println("Provide the information of parrot(s):");
            String parrotInput = reader.nextLine();
            String[] parrotInputList1 = parrotInput.split(",");
            Parrot[] parrots = new Parrot[parrotNum];
            for (int i = 0; i < parrotInputList1.length; i++) {
                String[] arr = parrotInputList1[i].split("\\|");
                String name = arr[0];
                int length = Integer.parseInt(arr[1]);
                parrots[i] = new Parrot(name, length);
            }
            
            for (int i = 0; i < parrots.length; i++) {
                parrotCages[i] = new Cage(parrots[i], parrots[i].getLength());
            }
        }

        System.out.print("hamster: ");
        int hamsterNum = reader.nextInt();
        reader.nextLine();
        Cage[] hamsterCages = new Cage[hamsterNum];
        if (hamsterNum != 0) {
            System.out.println("Provide the information of hamster(s):");
            String hamsterInput = reader.nextLine();
            String[] hamsterInputList1 = hamsterInput.split(",");
            Hamster[] hamsters = new Hamster[hamsterNum];
            for (int i = 0; i < hamsterInputList1.length; i++) {
                String[] arr = hamsterInputList1[i].split("\\|");
                String name = arr[0];
                int length = Integer.parseInt(arr[1]);
                hamsters[i] = new Hamster(name, length);
            }
            
            for (int i = 0; i < hamsters.length; i++) {
                hamsterCages[i] = new Cage(hamsters[i], hamsters[i].getLength());
            }
        }

        System.out.println("Animals have been successfully recorded!\n\n"
            + "=============================================\n"
            + "Cage arrangement:");

        if (catNum != 0) {
            ArrayList<ArrayList<Cage>> catArranged = CageArrange.arrange(catCages);
        }
        if (lionNum != 0) {
            ArrayList<ArrayList<Cage>> lionArranged = CageArrange.arrange(lionCages);
        }
        if (eagleNum != 0) {
            ArrayList<ArrayList<Cage>> eagleArranged = CageArrange.arrange(eagleCages);
        }
        if (parrotNum != 0) {
            ArrayList<ArrayList<Cage>> parrotArranged = CageArrange.arrange(parrotCages);
        }
        if (hamsterNum != 0) {
            ArrayList<ArrayList<Cage>> hamsterArranged = CageArrange.arrange(hamsterCages);
        }

        System.out.println("NUMBER OF ANIMALS:"
            + "\ncat:" + Cat.getCatNum()
            + "\nlion:" + Lion.getLionNum()
            + "\nparrot:" + Parrot.getParrotNum()
            + "\neagle:" + Eagle.getEagleNum()
            + "\nhamster:" + Hamster.getHamsterNum()
            + "\n\n=============================================");

        while (true) {
            System.out.println("Which animal you want to visit?"
                + "\n(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: Exit)");
            int input = reader.nextInt();
            reader.nextLine();
            if (input == 99) {
                return;

            } else if (input == 1) {
                System.out.print("Mention the name of cat you want to visit: ");
                String animalName = reader.nextLine();
                for (Cat var : Cat.getCatList()) {
                    if (var.getName().equals(animalName)) {
                        System.out.println("You are visiting " + animalName
                            + " (cat) now, what would you like to do?"
                            + "\n1: Brush the fur 2: Cuddle");
                        int actNum = reader.nextInt();
                        reader.nextLine();
                        if (actNum == 1) {
                            System.out.print("Time to clean " + animalName + "'s fur"
                                + "\n" + animalName + " makes a voice: ");
                            var.gotBrushed();
                        } else if (actNum == 2) {
                            System.out.print(animalName + " makes a voice: ");
                            var.gotCuddled();
                        } else {
                            System.out.println("You do nothing...");
                        }
                        System.out.println("Back to the office!\n");
                        break;
                    }
                    if (var == Cat.getCatList().get(Cat.getCatList().size()-1)) {
                        System.out.println("There is no cat with that name! Back to the office!\n");
                    }
                    
                }
                
            } else if (input == 2) {
                System.out.print("Mention the name of eagle you want to visit: ");
                String animalName = reader.nextLine();
                for (Eagle var : Eagle.getEagleList()) {
                    if (var.getName().equals(animalName)) {
                        System.out.println("You are visiting " + animalName
                            + " (eagle) now, what would you like to do?"
                            + "\n1: Order to fly");
                        int actNum = reader.nextInt();
                        reader.nextLine();
                        if (actNum == 1) {
                            System.out.print(animalName + " makes a voice: ");
                            var.fly();
                            System.out.println("You hurt!");
                        } else {
                            System.out.println("You do nothing...");
                        }
                        System.out.println("Back to the office!\n");
                        break;
                    }
                    if (var == Eagle.getEagleList().get(Eagle.getEagleList().size()-1)) {
                        System.out.println("There is no eagle with that name! Back to the office!\n");
                    }
                    
                }
                
            } else if (input == 3) {
                System.out.print("Mention the name of hamster you want to visit: ");
                String animalName = reader.nextLine();
                for (Hamster var : Hamster.getHamsterList()) {
                    if (var.getName().equals(animalName)) {
                        System.out.println("You are visiting " + animalName
                            + " (hamster) now, what would you like to do?"
                            + "\n1: See it gnawing 2: Order to run in the hamster wheel");
                        int actNum = reader.nextInt();
                        reader.nextLine();
                        if (actNum == 1) {
                            System.out.print(animalName + " makes a voice: ");
                            var.gnaw();
                        } else if (actNum == 2) {
                            System.out.print(animalName + " makes a voice: ");
                            var.runInWheel();
                        } else {
                            System.out.println("You do nothing...");
                        }
                        System.out.println("Back to the office!\n");
                        break;
                    }
                    if (var == Hamster.getHamsterList().get(Hamster.getHamsterList().size()-1)) {
                        System.out.println("There is no hamster with that name! Back to the office!\n");
                    }
                    
                }
            
            } else if (input == 4) {
                System.out.print("Mention the name of parrot you want to visit: ");
                String animalName = reader.nextLine();
                for (Parrot var : Parrot.getParrotList()) {
                    if (var.getName().equals(animalName)) {
                        System.out.println("You are visiting " + animalName
                            + " (parrot) now, what would you like to do?"
                            + "\n1: Order to fly 2: Do conversation");
                        int actNum = reader.nextInt();
                        reader.nextLine();
                        if (actNum == 1) {
                            System.out.print("Parrot " + animalName + " flies!"
                                + "\n" + animalName + " makes a voice: ");
                            var.fly();
                        } else if (actNum == 2) {
                            System.out.print("You say: ");
                            String message = reader.nextLine();
                            System.out.print(animalName + " says: ");
                            var.reply(message);
                        } else {
                            System.out.print(animalName + " says: ");
                            var.mumble();
                        }
                        System.out.println("Back to the office!\n");
                        break;
                    }
                    if (var == Parrot.getParrotList().get(Parrot.getParrotList().size()-1)) {
                        System.out.println("There is no parrot with that name! Back to the office!\n");
                    }
                    
                }
                
            
            } else if (input == 5) {
                System.out.print("Mention the name of lion you want to visit: ");
                String animalName = reader.nextLine();
                for (Lion var : Lion.getLionList()) {
                    if (var.getName().equals(animalName)) {
                        System.out.println("You are visiting " + animalName
                            + " (lion) now, what would you like to do?"
                            + "\n1: See it hunting 2: Brush the mane 3: Disturb it");
                        int actNum = reader.nextInt();
                        reader.nextLine();
                        if (actNum == 1) {
                            System.out.print("Lion is hunting.."
                                + "\n" + animalName + " makes a voice: ");
                            var.hunt();
                        } else if (actNum == 2) {
                            System.out.print("Clean the lion\'s mane..\n" 
                                + animalName + " makes a voice: ");
                            var.gotBrushed();
                        } else if (actNum == 3) {
                            System.out.print(animalName + " makes a voice: ");
                            var.gotDisturbed();
                        } else {
                            System.out.println("You do nothing...");
                        }
                        System.out.println("Back to the office!\n");
                        break;
                    }
                    if (var == Lion.getLionList().get(Lion.getLionList().size()-1)) {
                        System.out.println("There is no lion with that name! Back to the office!\n");
                    }
                    
                }
                
            
            } else {
                System.out.println("There is no animal of that number.");
            }
        }
        
    }
}