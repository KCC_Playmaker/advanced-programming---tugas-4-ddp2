public class Animal {
    protected boolean isWild;
    protected String name;
    protected int length;
    //protected Cage homeCage;

    public Animal(String name, int length) {
        this.name = name;
        this.length = length;
        //this.homeCage = homeCage;
    }

    public boolean isWild() {
        return this.isWild;
    }

    public String getName() {
        return this.name;
    }

    public int getLength() {
        return this.length;
    }
/*
    public Cage getHomeCage() {
        return this.homeCage;
    }*/
}