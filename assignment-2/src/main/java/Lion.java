import java.util.ArrayList;

public class Lion extends Animal {
    private static final String HUNT_VOICE = "err...!";
    private static final String BRUSHING_VOICE = "Hauhhmm!";
    private static final String DISTURBED_VOICE = "HAAHUM!!";
    private static ArrayList<Lion> lionList = new ArrayList<Lion>();
    private static int lionNum = 0;

    public Lion(String name, int length) {
        super(name, length);
        this.isWild = true;
        lionList.add(this);
        lionNum++;
    }

    public void hunt() {
        System.out.println(HUNT_VOICE);
    }

    public void gotBrushed() {
        System.out.println(BRUSHING_VOICE);
    }

    public void gotDisturbed() {
        System.out.println(DISTURBED_VOICE);
    }

    public String getHuntVoice() {
        return HUNT_VOICE;
    }

    public String getBrushingVoice() {
        return BRUSHING_VOICE;
    }

    public String getDisturbedVoice() {
        return DISTURBED_VOICE;
    }

    public static ArrayList<Lion> getLionList() {
        return lionList;
    }

    public static int getLionNum() {
        return lionNum;
    }
}