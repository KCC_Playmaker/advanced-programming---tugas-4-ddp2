import java.util.ArrayList;

public class Parrot extends Animal {
    private static final String FLY_VOICE = "FLYYYY.....";
    private static final String MUMBLE = "HM?";
    private static ArrayList<Parrot> parrotList = new ArrayList<Parrot>();
    private static int parrotNum = 0;

    public Parrot(String name, int length) {
        super(name, length);
        this.isWild = false;
        parrotList.add(this);
        parrotNum++;
    }

    public void fly() {
        System.out.println(FLY_VOICE);
    }

    public void reply(String speech) {
        if (speech.equals("")) {
            this.mumble();
        } else {
            System.out.println(speech.toUpperCase());
        }
    }

    public void mumble() {
        System.out.println(MUMBLE);
    }

    public String getFlyVoice() {
        return FLY_VOICE;
    }

    public String getMumble() {
        return MUMBLE;
    }

    public static ArrayList<Parrot> getParrotList() {
        return parrotList;
    }

    public static int getParrotNum() {
        return parrotNum;
    }
}