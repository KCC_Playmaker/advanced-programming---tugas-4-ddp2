import java.util.ArrayList;

public class Hamster extends Animal {
    private static final String GNAW = "Ngkkrit.. Ngkkrrriiit";
    private static final String WHEEL_RUN_VOICE = "trrr.... trrr...";
    private static ArrayList<Hamster> hamsterList = new ArrayList<Hamster>();
    private static int hamsterNum = 0;

    public Hamster(String name, int length) {
        super(name, length);
        this.isWild = false;
        hamsterList.add(this);
        hamsterNum++;
    }

    public void gnaw() {
        System.out.println(GNAW);
    }

    public void runInWheel() {
        System.out.println(WHEEL_RUN_VOICE);
    }

    public String getGnaw() {
        return GNAW;
    }

    public String getWheelRunVoice() {
        return WHEEL_RUN_VOICE;
    }

    public static ArrayList<Hamster> getHamsterList() {
        return hamsterList;
    }

    public static int getHamsterNum() {
        return hamsterNum;
    }
}