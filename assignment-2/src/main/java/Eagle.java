import java.util.ArrayList;

public class Eagle extends Animal {
    private static final String FLY_VOICE = "Kwaakk....";
    private static ArrayList<Eagle> eagleList = new ArrayList<Eagle>();
    private static int eagleNum = 0;

    public Eagle(String name, int length) {
        super(name, length);
        this.isWild = true;
        eagleList.add(this);
        eagleNum++;
    }

    public void fly() {
        System.out.println(FLY_VOICE);
    }

    public String getFlyVoice() {
        return FLY_VOICE;
    }

    public static ArrayList<Eagle> getEagleList() {
        return eagleList;
    }

    public static int getEagleNum() {
        return eagleNum;
    }
}