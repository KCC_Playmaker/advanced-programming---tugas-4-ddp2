import java.util.Random;
import java.util.ArrayList;

public class Cat extends Animal {
    private static final String BRUSHING_VOICE = "Nyaaan...";
    private static final String[] CUDDLING_VOICES = new String[]{"Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"};
    private static ArrayList<Cat> catList = new ArrayList<Cat>();
    private static int catNum = 0;
    Random randGenerator = new Random();

    public Cat(String name, int length) {
        super(name, length);
        this.isWild = false;
        catList.add(this);
        catNum++;
    }

    public void gotBrushed() {
        System.out.println(BRUSHING_VOICE);
    }

    public void gotCuddled() {
        int randNum = randGenerator.nextInt(CUDDLING_VOICES.length);
        System.out.println(CUDDLING_VOICES[randNum]);
    }

    public static String getBrushingVoice() {
        return BRUSHING_VOICE;
    }

    public static String[] getCuddlingVoices() {
        return CUDDLING_VOICES;
    }

    public static ArrayList<Cat> getCatList() {
        return catList;
    }

    public static int getCatNum() {
    	   return catNum;
    }
}